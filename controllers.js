var siteApp = angular.module('siteApp', []);
siteApp.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $httpProvider.defaults.headers.post = { 'Accept' : 'application/json, text/javascript' };
}]);

siteApp.controller('userController', function ($scope, $http) {

    $scope.orders = [];
    $scope.getOrders = function() {
        $http.get('http://shop4vj.loc/index.php?r=admin_panel/admin/AdminOrder')
            .success(function(data) {
                $scope.orders = data;
        });
    };
    $scope.vis = true;
    $scope.toggle = function() {
        $scope.vis = !$scope.vis;
    };
    $scope.edit = true;
    $scope.error = false;
    $scope.incomplete = false;
    $scope.orderProp = 'id';


    $scope.formData = {};
    $scope.editOrder = function(id) {
        if (id == 'new') {
                alert('New Order');
        } else {
            $scope.vis = false;
            $scope.edit = false;
            console.log($scope);
            $scope.formData.id = $scope.orders[id].id;
            $scope.formData.status = $scope.orders[id].status;
            $scope.formData.order_price = $scope.orders[id].order_price;
            $scope.formData.coupon = $scope.orders[id].coupon;
            $scope.formData.id_user = $scope.orders[id].id_user;
        }
    };


    $scope.save  = function() {
        $scope.url = 'http://shop4vj.loc/index.php?r=admin_panel/admin/AjaxSave';

        //var formData = new FormData($scope.user_form);
        //$scope.formData2 = [];
      /*  $.ajax({
            dataType:'JSON',
            type: 'POST',
            url: $scope.url,
            data: $scope.formData,
        success: function(data){

        },
        error: function(data) {
            alert('Error. Please try again later.');
            alert(data);
        }
    });*/
        var paramsVal={data:'"id":"1"'};
        $http({
            method: 'POST',
            url: $scope.url,
            responseType: 'json',
            params: paramsVal,
            withCredentials: true,
               // data: $scope.formData, //'email='+$scope.email+'&username='+$scope.username, // $scope.formData
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
        .success(function(data) {
                $scope.formData2 = data;
                console.log(  $scope.formData2);
            });
    };


/*    $scope.$watch('passw1', function() {$scope.test();});
    $scope.$watch('passw2', function() {$scope.test();});
    $scope.$watch('username', function() {$scope.test();});
    $scope.$watch('email', function() {$scope.test();});

    $scope.test = function() {
        if ($scope.passw1 !== $scope.passw2) {
            $scope.error = true;
        } else {
            $scope.error = false;
        }
        $scope.incomplete = false;
        if ($scope.edit && (!$scope.username.length ||
            !$scope.email.length ||
            !$scope.passw1.length || !$scope.passw2.length)) {
            $scope.incomplete = true;
        }
    };*/
});

