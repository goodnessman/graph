$(document).ready(function(){
	$.fn.exists = function(){return this.length > 0;}
	$('#to-top').click(function(){
		$.scrollTo( 0, 1500 );
		return false;
	});
	$('.to-about').click(function(){
		$.scrollTo( '#about', 2000, {offset:-59} );
		return false;
	});
	$('.to-service').click(function(){
		if($( window ).width() > 991){
			$.scrollTo( '#services', 2000, {offset:-59} );
		}else{
			$.scrollTo( '#services', 2000 );
		}
		return false;
	});
	$('.to-process').click(function(){
		if($( window ).width() > 991){
			$.scrollTo( '#process', 2000, {offset:-59} );
		}else{
			$.scrollTo( '#process', 2000 );
		}
		return false;
	});
	$('.to-details').click(function(){
			$.scrollTo( '#details', 1000, {offset:1} );
		return false;
	});
	$('.to-pages').click(function(){
			$.scrollTo( '#pages', 1000, {offset:1} );
		return false;
	});
	$('.to-features').click(function(){
			$.scrollTo( '#features', 1000, {offset:1} );
		return false;
	});
	$('.to-general-markup').click(function(){
			$.scrollTo( '#general-markup', 1000, {offset:1} );
		return false;
	});
	$('.to-grid-system').click(function(){
			$.scrollTo( '#grid-system', 1000, {offset:1} );
		return false;
	});
	$('.to-edit-color').click(function(){
			$.scrollTo( '#edit-color', 1000, {offset:1} );
		return false;
	});
	$('.to-email-address').click(function(){
			$.scrollTo( '#email-address', 1000, {offset:1} );
		return false;
	});
	$('.to-twitter-account').click(function(){
			$.scrollTo( '#twitter-account', 1000, {offset:1} );
		return false;
	});
	$('.to-css-structure').click(function(){
			$.scrollTo( '#css-structure', 1000, {offset:1} );
		return false;
	});
	$('.to-instagram').click(function(){
			$.scrollTo( '#instagram', 1000, {offset:1} );
		return false;
	});
	$('.to-typography').click(function(){
			$.scrollTo( '#typography', 1000, {offset:1} );
		return false;
	});
	$('.to-js-files').click(function(){
			$.scrollTo( '#js-files', 1000, {offset:1} );
		return false;
	});
	$('.to-materials-use').click(function(){
			$.scrollTo( '#materials-use', 1000, {offset:1} );
		return false;
	});
	$('.change-video').click(function(){
			$.scrollTo( '#change-video', 1000, {offset:1} );
		return false;
	});
	$('body.documentation-section').scrollspy({ offset: 0, target: '.side-bar' });
});