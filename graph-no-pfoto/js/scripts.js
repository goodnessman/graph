
"use strict";

var $ = jQuery.noConflict();

// functions

var reload_page = function() {
    location.reload();
};

$.fn.exists = function(){return this.length > 0;};

// close all poppup
var closePoppup = function(el) {
    if ( $(el).parents("#app_cart").length) {
        var app_cart = $('#app_cart');
       app_cart.find('.apper_poppup').addClass('slideRight');
       setTimeout(function() {
               app_cart.find('.apper_poppup')
               .removeClass('slideRight')
               .css('display', 'none');
       }, 800);
       return false;
    } else if ($('#app_menu').css('display') === 'block') {
        $('.header_wrap img').css('display','block');
        $('.header_wrap a').css('color','');
        $('.header_wrap .title').css('color','');
        $('.header_wrap_black').css('background-color', '');
        $('.cont_wrap').fadeOut(300);
        $('.apper_poppup').slideUp(500);
        return false;
    } else if ($('#app_seach').css('display') === 'block' ||
    $('#app_login').css('display') === 'block') {
        $('.header_wrap').css('display','');
        $('.cont_wrap').fadeOut(300);
       $('.apper_poppup').slideUp(500);
    }else {
       $('.cont_wrap').fadeOut(300);
       $('.apper_poppup').slideUp(500);
       return false;
    }
    return false;
};

function progect_poppup(elem) {
    var poppup = $(elem).parent('.progect_img').find('.poppup');
    var cell = $(elem).parent('.progect_img').find('.cont_cell');
    if ($(elem).find("img").length == 1){
        return false;
   }else{
        if($(elem).find("video").length == 1){
            var htmlVideo = $(elem).html();
            cell.append(htmlVideo);
            poppup.fadeIn(300)
            .css({'background':'rgba(0, 0, 0, 0.9);'})
            .find('.cross')
            .css('background','url("images/image_100.png") no-repeat center');
            poppup.find('.social_sidebar').fadeOut();
        }
   }
}

function tumbler_features(elem) {
    var text = $(elem).parent('.title').parent('.block').find('.text');
    var tumbler = $(elem);
    if(text.css('display') == 'none') {
        text.slideDown(500);
        tumbler.find('.plus').fadeOut(1);
        tumbler.find('.minus').fadeIn(1);
    }else {
        text.slideUp(500);
        tumbler.find('.plus').fadeIn(1);
        tumbler.find('.minus').fadeOut(1);
    }
}

// about page: hover for four_icon 
var show_text = function(elem) {
    var block = $(elem);
    var text_out = block.find('.text_out').html();
    var text_in = block.parent('.four_icon').find('.text_in');
    text_in.ContentText = "";
    text_in.fadeOut(0);
    text_in.fadeIn(500).html(text_out);
    block.parent('.four_icon').find('.block .bottom').css('visibility', 'hidden');
    block.find('.bottom').css('visibility', 'visible');
    $('.graph_blockquote').css('margin','146px auto 0');
};

var blogSocial = function(elem) {
    var ul = $(elem).parent('.social').find('ul');
    if (ul.hasClass('hide')) {
        ul.show(100,function() {
            ul.removeClass('hide');
        });
    }else {
        ul.hide(600);
        setTimeout(function(){ul.addClass('hide');}, 600);
    }
};

// shop_checkout page: toogle visibiliti blocks
var toogleVisibilitu =function(e) {
    $(e).parent('.click_wrap').find('.hidden_block').fadeToggle(500);
};



// video 
function vidplay_01() {
    var video = document.getElementById('video-block');
    $('.video').toggleClass('play').find('.overlay').fadeToggle();
        if (video.paused) {
            video.play();
            video.addEventListener('ended', function(){
                video.load();
                $('.video').toggleClass('play').find('.overlay').fadeToggle();
            });
        }
        else {
            video.pause();
        }
    }



// bx sliders

$('.index_slider').bxSlider({
    delay: 5000,
  // mode: 'fade',
  captions: true,
  controls: false,
  auto: true
});

$('.index4_slider').bxSlider({
    delay: 5000,
  // mode: 'fade',
  captions: true,
  controls: false,
  auto: true
});

$('.about_slider_1').bxSlider({
    delay: 5000,
  // mode: 'fade',
  captions: true,
  controls: false,
  auto: true
});

$(document).ready(function(){
  $('.about_slider_2').bxSlider({
    slideWidth: 200,
    minSlides: 5,
    maxSlides: 5,
    moveSlides: 1,
    slideMargin: 10,
    pager: false,
    auto: true
  });
});

$('.shopDetail_slider').bxSlider({
  pagerCustom: '.shopDetail_slider_pager',
  controls: false,
  auto: true
});

// eqvival height
$('.services_page .item').matchHeight();

$('.blog .item').matchHeight();

$('.blog_detail .blog_blocks .item').matchHeight();

$('.contact_2 .content_wrap .item').matchHeight();  

$('.services_page .blocks .item').matchHeight();


// project inside content slider
if ($('.progect_img').exists()) {

    $(".progect_img .img").each(function(){
        var src = $(this).children("img").attr("src");
        $(this).append('<div class="bg"></div>');
        $(this).children(".bg").css("background-image","url('"+src+"')");
    });


    // slider
    $(function () {
      var list = $('[data-org-img]');
      list.on('click', function (e) {
        var current = $(this);
          var poppup =  current.parent('.img').parent('.progect_img').find('.poppup');
          var link = current.attr('src');
          var cell = current.parent('.img').parent('.progect_img').find('.cont_cell');
          if(cell.find("img").length === 0) {
              cell.append('<img src='+link+' />');
              poppup.fadeIn(300)
                .css({'background':'#fff'})
                .find('.cross')
                .css('background','url("images/image_15.png") no-repeat center');
                poppup.find('.social_sidebar').fadeIn();
          }
          if ($('.portfolio_detail .progect_img .social_sidebar').exists()) {
            $('.portfolio_detail .progect_img .social_sidebar .right').on('click', function() {
                var link = current.attr('src');
                var current_img = list.index(current);
                if(current_img == list.length-1){
                    current_img = 0;
                }
                var next = list.eq(current_img + 1);
                var next_link = next.attr('src');
                cell.html('').append('<img src='+next_link+' />');
                current = next;
            });
            $('.portfolio_detail .progect_img .social_sidebar .left').on('click', function() {
                var link = current.attr('src');
                var current_img = list.index(current);
                if(current_img === 0){
                    current_img = list.length-1;
                }
                var next = list.eq(current_img - 1);
                var next_link = next.attr('src');
                cell.html('').append('<img src='+next_link+' />');
                current = next;
            });
          }
      });
    });
    $('.progect_img .cross').on('click', function() {
        var poppup = $('.progect_img .poppup');
        var cell = $('.progect_img .cont_cell');
        poppup.fadeOut(300)
        .css({'background':''})
        .find('.cross')
        .css('background','');
        poppup.find('.cont_cell').html('');
        return false;
    });
}


// video on click

$('.video .overlay').on('click', function() {
     $('.video .play').on('click');
     $('.video .overlay').css('visibility','hidden ');
});

    $('.video .overlay .link_page').on('click', function() {
        window.location.href = $(this).attr("href");
        return false;
});


// app menu

var app_menu = $('#app_menu');
var app_login = $('#app_login');
var app_search = $('#app_seach');
var app_singUp = $('#app_singUp');
var app_cart = $('#app_cart');

$('.header_wrap .cart_opener').on('click',  function() {
    if (app_cart.find('.apper_poppup').hasClass('active')) {
        app_cart.find('.apper_poppup').addClass('slideRight');
        setTimeout(function() {
                app_cart.find('.apper_poppup')
                .removeClass('slideRight')
                .removeClass('active')
                .css('display', 'none');
        }, 800);
        return false;
    }else {
        app_cart.find('.apper_poppup').addClass('slideLeft').addClass('active');
        setTimeout(function() {
                app_cart.find('.apper_poppup')
                .removeClass('slideLeft')
                .css('display', 'block');
        }, 2000);
        return false;
    }
});

$('.header_wrap .menu_opener').on('click',  function() {
    app_menu.find('.cont_wrap').fadeIn(1000);
    $('.header_wrap img').css('display','none');
    $('.header_wrap a').css('color','#fff');
    $('.header_wrap .title').css('color','#000');
    $('.header_wrap_black').css('background-color', '#fff');
    app_menu.slideDown(500);
    return false;
});

$('.header_wrap .login_opener').on('click', function() {
    app_login.find('.cont_wrap').fadeIn(1000);
    $('.header_wrap').css('display','none');
    app_login.slideDown(500);
    return false;
});
$('#login_opener2').on('click', function() {
    app_singUp.fadeOut(100);
    app_login.find('.cont_wrap').fadeIn(1000);
    $('.header_wrap').css('display','none');
    app_login.slideDown(500);
    return false;
});

$('.header_wrap .search_opener').on('click', function() {
    app_search.find('.cont_wrap').fadeIn(1000);
    $('.header_wrap').css('display','none');
    app_search.slideDown(500);
    return false;
});

 $('.singUp_opener').on('click', function() {
    if($('#app_login').css('display') === 'block') {
        app_login.fadeOut(1);
    }
    app_singUp.find('.cont_wrap').fadeIn(1000);
    app_singUp.slideDown(500);
    return false;
});



// sort blocks

if ($('#isotop_container').exists()) {
    var $isotopContainer = $('#isotop_container');
    var    isotopItemMax = 16;
    var    small = $('#filter_small');
    var    medium = $('#filter_medium');
    var    img = $('#isotop_container img');

    if ($('.portfolio_masonry_3').exists()) {
        isotopItemMax = 12;
    }
    if ($('.index_2') ) {
        $isotopContainer.css('position', 'relative');
        setTimeout(function() {
            $isotopContainer.isotope({
                    itemSelector: '.item',
                    filter: ':nth-child(-n+' + isotopItemMax + ')'
                });
        }, 3000);
    }else {
        $isotopContainer.css('position', 'relative');
        setTimeout(function() {
            $isotopContainer.isotope({
                    itemSelector: '.item',
                    filter: ':nth-child(-n+' + isotopItemMax + ')'
                });
        }, 1000);
    }
    
    $('#filter_small').on('click', function() {
        if(!small.hasClass('active')) {
            small.css('opacity', 1).addClass('active');
            medium.css('opacity', 0.2).removeClass('active');

            $isotopContainer.find('.item').removeClass('item_class').css('width', '33.3%');
            $isotopContainer.isotope({
                itemSelector: '.item',
                filter: ':nth-child(-n+' + isotopItemMax + ')'
            });
        }
    });
    $('#filter_medium').on('click', function() {
        if(!medium.hasClass('active')) {
            medium.css('opacity', 1).addClass('active');
            small.css('opacity', 0.2).removeClass('active');
            $isotopContainer.find('.item').css('width', '25%');
            $isotopContainer.isotope({
                itemSelector: '.item',
                filter: ':nth-child(-n+' + isotopItemMax + ')'
            });
        }
    });
    $('#load_more').on('click', function(){
            isotopItemMax += 16;
            $isotopContainer.isotope({filter: ':nth-child(-n+' + isotopItemMax + ')'});
            return false;
    });
    $('.filter a').on('click', function(){
        $(this).parent('li').addClass('active').siblings('li').removeClass('active');
        var selector = $(this).attr('data-filter');
        $isotopContainer.isotope({ filter: selector });
        return false;
    });
}







// header


$(window).on('scroll', function(){
    
    if ($('#app_menu').css('display') != 'block' &&
    $('#app_login').css('display') != 'block' &&
    $('#app_seach').css('display') != 'block') {
        var tempScrollTop = 0;
        var currentScrollTop = 0;
        var headerWhite = $('.header_wrap_white');
        var headerBlack = $('.header_wrap_black');
        var currentScrollTop = $(window).scrollTop();
            headerBlack.css('background-color','#000');
            headerBlack.find('.header').css('color', '#fff');
            headerBlack.find('a').css({'color': '#fff'});
        if (tempScrollTop < currentScrollTop ){
            headerWhite.fadeOut(300);
            headerBlack.fadeIn(400);
        }  else {
            headerBlack.fadeOut(300);
            headerWhite.fadeIn(500);
        }
    }
});


// project inside page slider

if ($('.portfolio_detail .page_slider .social_sidebar').exists()) {

    $('.portfolio_detail .page_slider .social_sidebar .right').on('click', reload_page);

    $('.portfolio_detail .page_slider .social_sidebar .left').on('click', reload_page);

    if ($('.progect_description').exists()) {
        var description = $('.progect_description');
        var wrap_cont = $('.progect_description .description');
        $('.portfolio_detail .page_slider .social_sidebar .info').on('click', function() {
            if(description.hasClass('.hidden')) {
                $(this).css('text-decoration', 'line-through');
                wrap_cont.fadeIn(100);
                description.slideDown(500).removeClass('.hidden');
            }else {
                $(this).css('text-decoration', '');
                wrap_cont.fadeOut(200);
                description.slideUp(500).addClass('.hidden');
            }
        });
    }

    $('.portfolio_detail .social_sidebar .button').on('click', function() {
        var button = $('.portfolio_detail .social_sidebar .button');
        var social = $('.portfolio_detail .social_sidebar .social');
        if(button.hasClass('.hidden')) {
            social.slideDown(500);
            button.css('border','1px solid #000').removeClass('.hidden');
        }else {
            social.slideUp(500);
            button.css('border','').addClass('.hidden');
        }
    });
}


$(window).load(function(){

    // socias sidebar

    if ($('.portfolio_detail .social_sidebar').exists()) {
        $('.portfolio_detail .social_sidebar .social').slideUp(100);
        $('.portfolio_detail .social_sidebar .button').css('border','').addClass('.hidden');
    }


    // sort blocks

    if ($('.portfolio_masonry_3').exists() || $('portfolio_masonry_3').exists()) {

        var $isotopContainer = $('#isotop_container'),
            small = $('#filter_small'),
            medium = $('#filter_medium'),
            img = $('#isotop_container img');
        $isotopContainer.css('position', 'relative');

            small.css('opacity', 1).addClass('active');
            medium.css('opacity', 0.2).removeClass('active');
            $isotopContainer.find('.item').removeClass('item_class').css('width', '33.3%');
            $isotopContainer.isotope({
                itemSelector: '.item',
                filter: ':nth-child(-n+' + isotopItemMax + ')'
            });

    }




    // 
    function twitterFrame(){
        var a = 0;
        function twitter_frame1(){
            setTimeout(function(){
                var html_frame = document.getElementById('twitter-widget-0').contentWindow.document.body.innerHTML;
                
                $('.hide_content_twitter').html(html_frame);
                $('.hide_content_twitter .root>.stream').find('.h-feed>li').each(function(){
                    var author = $(this).find(".p-nickname").html();
                    var text = $(this).find(".e-entry-title").html();
                    var time_ago = $(this).find(".dt-updated").html();
                    var post_link = $(this).find(".u-url.permalink.customisable-highlight").attr('href');
                    var author_link = $(this).find(".u-url.profile").attr('href');
                    $("#twitter-fade").append('<li><div><h2>' + time_ago + '</h2>' + '<cite><a href="https://twitter.com/Geniustudio">follow' + author + '</a></cite>' + '<p>' + text + '</p></div>');
                });
                var b = $('#twitter-fade li');
                if (b.length > 0) {
                    if ($('#twitter-fade').exists()) {
                        $('#twitter-fade').bxSlider({
                            mode: 'fade',
                            captions: true,
                            controls: false,
                            auto: true,
                        });
                    }
                    return false;
                }
                else
                if (a === 300) {
                    return false;
                }else{
                    a += 1;
                    twitter_frame1();
                }
            }, 300);
        }
        twitter_frame1();
    }
    if ($('.hide_content_twitter').exists()){
        twitterFrame();
    }
});


    


function index3ImgHoverOver(element) {
   $(element).find('.overlay').css('background','none');
}

function index3ImgHoverOut(element) {
    $(element).find('.overlay').css('background','');
}



// about page: ran scills numbers
function counterUpNumber(line) {
    var line = document.querySelector(line);
    var value = parseInt(line.getAttribute('data-scill'), 10);
    var i = 1;
    var timerId = setTimeout(function go() {
        line.style.width = i + '%';
        line.parentNode.querySelector('span').textContent = i + '%';
        if (i < value) setTimeout(go, 50);
        i++;
      }, 50);
}

// about page: check time for ran scills numbers
$('.scills').each(function () {
    var block = $(this);
    $(window).on('scroll', function() {
        var top = block.offset().top;
        var padding = parseInt(block.css('paddingTop'), 10) + parseInt(block.css('paddingBottom'), 10);
        var bottom = block.height()+top+padding - 300;
        top = top - $(window).height() + 200;
        var scroll_top = $(this).scrollTop();
        if ((scroll_top > top)) {
            if (!block.hasClass('animated')) {
                block.addClass('animated');
                counterUpNumber('.scill_1');
                counterUpNumber('.scill_2');
                counterUpNumber('.scill_3');
            }
        } else {
            block.removeClass('animated');
        }
    });
});

// index_4 page: parallax
if ($('.index_4')) {
    $('#parallaxBlock1').parallax("50%", 0.01);
    $('#parallaxBlock2').parallax("50%", 0.01);
    $('#parallaxBlock3').parallax("50%", 0.01);
    $('#parallaxBlock4').parallax("50%", 0.01);
    $('#parallaxBlock5').parallax("50%", 0.01);
    $('#parallaxBlock6').parallax("50%", 0.01);
}
